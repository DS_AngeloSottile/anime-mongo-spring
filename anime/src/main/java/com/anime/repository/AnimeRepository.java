package com.anime.repository;

import com.anime.model.Anime;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AnimeRepository  extends MongoRepository<Anime,String> {

    @Query("{ 'name': ?0 }")
    Optional<Anime> findByName(String animeName);

    @Aggregation(pipeline = {
            "{$match: {'numberEpisodes': { $gte: 30, $lt: 36 }}}",
            "{$sort: { 'name': 1 }}"
    })
    List<Anime> exampleAggregationFindByEpisodes();
}
