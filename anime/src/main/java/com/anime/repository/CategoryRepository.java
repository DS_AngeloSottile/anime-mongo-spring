package com.anime.repository;

import com.anime.model.Category;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends MongoRepository<Category,String> {

    @Query("{ 'name': ?0 }")
    Optional<Category> findByName(String categoryName);
}
