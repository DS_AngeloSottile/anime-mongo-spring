package com.anime.exception;

public class EventSendingException extends RuntimeException{

    public EventSendingException(String message) {
        super(message);
    }
    
    public EventSendingException(String message, Throwable e) {
        super(message,e);
    }


}
