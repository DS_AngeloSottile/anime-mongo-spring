package com.anime.exception;

public interface GException {

    Integer getCode();
    String getMessage();
}
