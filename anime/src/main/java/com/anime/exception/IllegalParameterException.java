package com.anime.exception;

public class IllegalParameterException extends RuntimeException implements GException{
    private final Integer code;

    public IllegalParameterException(String message, Integer code) {
		super(message);
		this.code = code;
	}

	@Override
	public Integer getCode() {
	  return this.code;
	}
}
