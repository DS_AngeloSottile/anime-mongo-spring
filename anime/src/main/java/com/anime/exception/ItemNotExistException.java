package com.anime.exception;

public class ItemNotExistException extends RuntimeException implements GException{

    private final Integer code;

    public ItemNotExistException(String messageString, Integer code) {
		super(messageString);
		this.code = code;
	}

	@Override
	public Integer getCode() {
	  return this.code;
	}
    
}
