package com.anime.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AnimeSyncSendDto {
    private String name;
    private String duration;
    private int numberEpisodes;
}
