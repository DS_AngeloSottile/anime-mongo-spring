package com.anime.model.dto;

import com.anime.model.Company;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AnimeStoreDto {

    private String id;
    private String name;
    private String duration;
    private int numberEpisodes;
    private Company company;
    private List<String> categories;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime transmissionDate;
}
