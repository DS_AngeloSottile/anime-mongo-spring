package com.anime.model.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AnimeUpdateRequestDto {
    private String name;
    private String duration;
    private int numberEpisodes;
}
