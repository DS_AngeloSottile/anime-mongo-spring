package com.anime.service;

import com.anime.model.Category;

public interface CategoryService {

    Category storeCategory(Category category);

    Category findCategoryByName(String categoryName);
    Category findCategoryByNameMongodbOperations(String categoryName);
}
