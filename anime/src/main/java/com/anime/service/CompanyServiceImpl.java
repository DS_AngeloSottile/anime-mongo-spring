package com.anime.service;

import com.anime.exception.IllegalParameterException;
import com.anime.exception.ItemNotExistException;
import com.anime.model.Company;
import com.anime.repository.CompanyRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class CompanyServiceImpl implements CompanyService{

    @Autowired
    CompanyRepository companyRepository;
    @Override
    public Company storeCompany(Company company) {
        if(StringUtils.isAllBlank(company.getName())){

            throw new IllegalParameterException("name cannot be null", HttpStatus.INTERNAL_SERVER_ERROR.value());
        }

        company.setCreationDate(LocalDateTime.now());
        company.setUpdateDate(LocalDateTime.now());

        return companyRepository.save(company);
    }

    public Company findCompanyByName(String categoryName){

        Optional<Company> companyOptional = companyRepository.findByName(categoryName);

        if(companyOptional.isEmpty()){
            throw new ItemNotExistException(String.format("Company not exist %s",categoryName),HttpStatus.INTERNAL_SERVER_ERROR.value());
        }

        return companyOptional.get();
    }
}
