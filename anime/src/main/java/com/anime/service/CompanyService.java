package com.anime.service;

import com.anime.model.Category;
import com.anime.model.Company;

public interface CompanyService {

    Company storeCompany(Company company);

    Company findCompanyByName(String categoryName);
}
