package com.anime.service;

import com.anime.controller.converter.AnimeConverter;
import com.anime.exception.IllegalParameterException;
import com.anime.exception.ItemNotExistException;
import com.anime.model.Anime;
import com.anime.model.Category;
import com.anime.model.Company;
import com.anime.model.dto.AnimeResponseDto;
import com.anime.model.dto.AnimeStoreDto;
import com.anime.model.dto.AnimeSyncSendDto;
import com.anime.model.dto.AnimeUpdateRequestDto;
import com.anime.repository.AnimeRepository;
import com.anime.service.kafka.message.KafkaMessageService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class AnimeServiceImpl implements AnimeService{

    @Autowired
    AnimeRepository animeRepository;
    @Autowired
    CompanyService companyService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    AnimeConverter animeConverter;
    @Autowired
    KafkaMessageService kafkaMessageService;
    @Autowired
    MongoOperations mongoOperations;
    @Autowired
    ObjectMapper objectMapper;

    @Override
    public Anime storeAnime(AnimeStoreDto animeRequest) {

        if(animeRequest == null || StringUtils.isAllBlank(animeRequest.getName())){
            throw new IllegalParameterException("illegal argument exception custom", HttpStatus.INTERNAL_SERVER_ERROR.value());
        }

        Anime anime = new Anime();
        anime.setDuration(animeRequest.getDuration());
        anime.setName(animeRequest.getName());
        anime.setNumberEpisodes(animeRequest.getNumberEpisodes());
        anime.setTransmissionDate(animeRequest.getTransmissionDate());

        if(animeRequest.getCategories() != null || !animeRequest.getCategories().isEmpty()){
            anime.setCategories(new ArrayList<>());
            for (String categoryName : animeRequest.getCategories() ) {

                Category category = categoryService.findCategoryByName(categoryName);
                anime.getCategories().add(category);
            }
        }

        if (animeRequest.getCompany() != null){

            Company company = companyService.findCompanyByName(animeRequest.getCompany().getName());
            anime.setCompany(company);
        }

        anime.setCreationDate(LocalDateTime.now());
        anime.setUpdateDate(LocalDateTime.now());

        kafkaMessageService.syncSend(generateEventToSend(anime),"animeToReview");

        return animeRepository.save(anime);
    }

    @Override
    public AnimeResponseDto updateAnimeByName(AnimeUpdateRequestDto animeRequest) {

        Optional<Anime> anime = animeRepository.findByName(animeRequest.getName());

        if(anime.isEmpty()){
            String messageError = String.format("The anime you selected %s does not exist",animeRequest.getName());
            throw new ItemNotExistException(messageError,HttpStatus.INTERNAL_SERVER_ERROR.value());
        }

        anime.get().setDuration(animeRequest.getDuration());
        anime.get().setNumberEpisodes(animeRequest.getNumberEpisodes());
        animeRepository.save(anime.get());

        return animeConverter.entity2Dto(anime.get());
    }

    @Override
    public List<AnimeResponseDto> findByNameMongoDb(String name) {
        Criteria baseCriteria = Criteria.where("name").is(name);
        Query queryWhere = Query.query(baseCriteria);

        List<Anime> animeList =  mongoOperations.find(queryWhere,Anime.class);
        if(animeList.isEmpty()){
             throw  new ItemNotExistException(String.format("No anime with name %s",name),HttpStatus.INTERNAL_SERVER_ERROR.value());
        }

        return animeConverter.entity2DtoList(animeList);
    }


    @Override
    public Map<String,Object> storeAnimeMongoOperations(AnimeStoreDto animeRequest) {

        Object toSave = objectMapper.convertValue(animeRequest,Anime.class);
        mongoOperations.insert(toSave,"anime");
        return objectMapper.convertValue(animeRequest, new TypeReference<>() { });
    }

    @Override
    public List <AnimeResponseDto> exampleAggregationFindByRange() {

        List<Anime> animeList = animeRepository.exampleAggregationFindByEpisodes();
        return animeConverter.entity2DtoList(animeList);
    }

    private AnimeSyncSendDto generateEventToSend(Anime anime){
        return AnimeSyncSendDto.builder()
                .name(anime.getName())
                .duration(anime.getDuration())
                .numberEpisodes(anime.getNumberEpisodes())
                .build();
    }
}
