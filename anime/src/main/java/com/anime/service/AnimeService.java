package com.anime.service;

import com.anime.model.Anime;
import com.anime.model.dto.AnimeResponseDto;
import com.anime.model.dto.AnimeStoreDto;
import com.anime.model.dto.AnimeUpdateRequestDto;

import java.util.List;
import java.util.Map;

public interface AnimeService {
    Anime storeAnime(AnimeStoreDto anime);

    AnimeResponseDto updateAnimeByName(AnimeUpdateRequestDto anime);

    List<AnimeResponseDto> findByNameMongoDb(String name);
    Map<String,Object> storeAnimeMongoOperations(AnimeStoreDto animeRequest);
    List <AnimeResponseDto> exampleAggregationFindByRange();
}
