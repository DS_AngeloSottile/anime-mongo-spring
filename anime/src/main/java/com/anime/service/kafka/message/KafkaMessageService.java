package com.anime.service.kafka.message;

import com.anime.exception.EventSendingException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.concurrent.ExecutionException;

@Service
public class KafkaMessageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaMessageService.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void syncSend(Object payload,String topic){

        try {

            //l'oggetto che arriva va inviato come stringa
            String payloadConvertedToString = mapper.writeValueAsString(payload);

            trySend(payloadConvertedToString,true,topic);
        } catch (JsonProcessingException e) {
            String message = String.format("Send Event Error (%s)", payload.getClass().getSimpleName());
            LOGGER.error(message,e);
            throw new EventSendingException(message,e);
        }
    }

    public void asyncSend(Object payload,String topic){

        try {
            //l'oggetto che arriva va inviato come stringa
            String payloadConvertedToString = mapper.writeValueAsString(payload);
            trySend(payloadConvertedToString,false,topic);
        } catch (JsonProcessingException e) {
            String message = String.format("Send Event Error (%s)", payload.getClass().getSimpleName());
            LOGGER.error(message,e);
            throw new EventSendingException(message,e);
        }

    }

    private void trySend(String payloadConvertedToString,boolean blocking,String topic) {
        boolean done = false;
        String errorMessage = null;
        int attempts = 10;
        for(int i = 0; i < attempts; i++){
            try {
                //altrimenti ListenableFuture<SendResult<K, V>>​                            //questo sarebbe l'output
                ListenableFuture<SendResult<String, String>> result = kafkaTemplate.send(topic,payloadConvertedToString);
                if(blocking){
                    result.get();
                }

                done = true;
                //se è stato mandato correttamente il ciclo for viene interotto
                break;
            } catch (InterruptedException e) {
                LOGGER.error("Message error", e);
                //se si ha un Interrupt exception interrompo il thread("se sto spegnendo l'applicazione")
                Thread.currentThread().interrupt();
            } catch ( ExecutionException e) {
                LOGGER.error("Message error", e);
                errorMessage = e.getLocalizedMessage();
                sleep();
            }
        }
        if(!done){
            throw new EventSendingException(errorMessage);
        }
    }

    private static void sleep() {
        try {
            int time = 5000;
            Thread.sleep(time);
        } catch (InterruptedException e1) {
            Thread.currentThread().interrupt();
        }
    }


}
