package com.anime.service;

import com.anime.exception.IllegalParameterException;
import com.anime.exception.ItemNotExistException;
import com.anime.model.Category;
import com.anime.repository.CategoryRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService{

    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    MongoOperations mongoOperations;

    @Override
    public Category storeCategory(Category category) {

        if(StringUtils.isAllBlank(category.getName())){
            throw new IllegalParameterException("name cannot be null", HttpStatus.INTERNAL_SERVER_ERROR.value());
        }

        category.setCreationDate(LocalDateTime.now());
        category.setUpdateDate(LocalDateTime.now());

        return categoryRepository.save(category);
    }


    public Category findCategoryByName(String categoryName){

        Optional<Category> categoryOptional = categoryRepository.findByName(categoryName);

        if(categoryOptional.isEmpty()){
            throw new ItemNotExistException("Category not exist",HttpStatus.INTERNAL_SERVER_ERROR.value());
        }

        return categoryOptional.get();
    }

    @Override
    public Category findCategoryByNameMongodbOperations(String categoryName) {
        Query where = new Query(Criteria.where("name").is(categoryName));

        return mongoOperations.findOne(where,Category.class);
    }
}
