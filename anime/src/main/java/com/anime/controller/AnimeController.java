package com.anime.controller;

import com.anime.model.Anime;
import com.anime.model.dto.AnimeResponseDto;
import com.anime.model.dto.AnimeStoreDto;
import com.anime.model.dto.AnimeUpdateRequestDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

public interface AnimeController {

    @Operation(summary = "Create a new Anime")
    @ApiResponses(value = {
            @ApiResponse(
            responseCode = "200",
            description = "the method create a new Anime",
            content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = Anime.class))
            }),
    })
    ResponseEntity<Anime> storeAnime(@RequestBody AnimeStoreDto animeRequest);

    @Operation(summary = "Update an existing anime by searching it by name")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "the method update an existing anime with data passed by request",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = Anime.class))
                    }),
    })
    ResponseEntity<AnimeResponseDto> updateAnimeByName(@RequestBody AnimeUpdateRequestDto animeRequest);


    @Operation(summary = "Update an existing anime by searching it by name")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "return a list of anime by name",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = Anime.class))
                    }),
    })
    ResponseEntity<List<AnimeResponseDto>> findByNameMongoDb(@PathVariable String name);

    @Operation(summary = "Create anime by request <-> mongo operations")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = Anime.class))
                    }),
    })
    ResponseEntity<Map<String, Object>> storeAnimeMongoOperations(@RequestBody AnimeStoreDto animeRequest);

    @Operation(summary = "Find anime in range of episodes numbers")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = Anime.class))
                    }),
    })
    ResponseEntity<List<AnimeResponseDto>> findByEpisodes();
}
