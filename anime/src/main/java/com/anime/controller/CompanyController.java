package com.anime.controller;

import com.anime.model.Anime;
import com.anime.model.Company;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

public interface CompanyController {
    @Operation(summary = "Create a new Company")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "the method create a new Company",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = Anime.class))
                    }),
    })
    ResponseEntity<Company> storeCompany(@RequestBody Company companyRequest);

    @Operation(summary = "Update an existing company")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "the method update an existing company",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = Anime.class))
                    }),
    })
    ResponseEntity<Company> updateCompany(Company companyRequest);
}
