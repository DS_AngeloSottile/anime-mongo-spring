package com.anime.controller;

import com.anime.model.Anime;
import com.anime.model.Category;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

public interface CategoryController {
    @Operation(summary = "Create a new Company")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "the method create a new Company",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = Anime.class))
                    }),
    })
    ResponseEntity<Category> storeCategory( @RequestBody Category categoryRequest);

    @Operation(summary = "Update an existing category")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "the method update an existing category",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = Anime.class))
                    }),
    })
    ResponseEntity<Category> updateCategory(@RequestBody Category categoryRequest);

    @Operation(summary = "Update an existing category")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "the method update an existing category",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = Anime.class))
                    }),
    })
    ResponseEntity<Category> findCategoryByNameMongodbOperations(@RequestBody Category categoryRequest);
}
