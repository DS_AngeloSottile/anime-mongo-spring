package com.anime.controller.error;

import com.anime.exception.GException;
import com.anime.exception.IllegalParameterException;
import com.anime.exception.ItemNotExistException;
import com.anime.model.dto.ErrorDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice(basePackages = "com.anime.controller")
public class ErrorHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandler.class);

    @ExceptionHandler(IllegalParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleIllegalParameterException(IllegalParameterException exception) {

      LOGGER.info("error logger");

      return generateError(exception);
    }

    @ExceptionHandler(ItemNotExistException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleItemNotExistException(ItemNotExistException exception) {

      LOGGER.info("error logger");

      return generateError(exception);
    }

    private ErrorDto generateError (GException exception){

      return ErrorDto.builder()
              .message(exception.getMessage())
              .code(exception.getCode())
              .build();
    }
}
