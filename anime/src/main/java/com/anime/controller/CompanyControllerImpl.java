package com.anime.controller;

import com.anime.model.Company;
import com.anime.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/company")
public class CompanyControllerImpl implements CompanyController {

    @Autowired
    CompanyService companyService;

    @Override
    @PostMapping(value = "/storeCompany")
    public ResponseEntity<Company> storeCompany(Company companyRequest) {

        Company company = companyService.storeCompany(companyRequest);

        return new ResponseEntity<>(company, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Company> updateCompany(Company companyRequest) {
        return null;
    }
}
