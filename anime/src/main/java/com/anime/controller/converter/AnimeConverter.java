package com.anime.controller.converter;

import com.anime.model.Anime;
import com.anime.model.dto.AnimeResponseDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AnimeConverter {

    public AnimeResponseDto entity2Dto(Anime anime) {

        return  AnimeResponseDto.builder()
                .name(anime.getName())
                .duration(anime.getDuration())
                .numberEpisodes(anime.getNumberEpisodes())
                .company(anime.getCompany())
                .categories(anime.getCategories())
                .transmissionDate(anime.getTransmissionDate())
                .creationDate(anime.getCreationDate())
                .updateDate(anime.getUpdateDate())
                .build();
    }

    public List<AnimeResponseDto> entity2DtoList(List<Anime> animeList) {

        List<AnimeResponseDto> animeDtoList = new ArrayList<>();
        for (Anime anime : animeList) {
            animeDtoList.add(
                    AnimeResponseDto.builder()
                    .name(anime.getName())
                    .duration(anime.getDuration())
                    .numberEpisodes(anime.getNumberEpisodes())
                    .company(anime.getCompany())
                    .categories(anime.getCategories())
                    .transmissionDate(anime.getTransmissionDate())
                    .creationDate(anime.getCreationDate())
                    .updateDate(anime.getUpdateDate())
                    .build());
        }

        return  animeDtoList;
    }

}
