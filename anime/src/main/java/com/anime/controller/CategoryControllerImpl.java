package com.anime.controller;

import com.anime.model.Category;
import com.anime.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/category")
public class CategoryControllerImpl implements CategoryController{

    @Autowired
    CategoryService categoryService;

    @Override
    @PostMapping(value = "/storeCategory")
    public ResponseEntity<Category> storeCategory(Category categoryRequest) {

        Category category = categoryService.storeCategory(categoryRequest);

        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Category> updateCategory(Category categoryRequest) {
        return null;
    }

    @Override
    @GetMapping(value = "/findByNameMongodb")
    public ResponseEntity<Category> findCategoryByNameMongodbOperations(Category categoryRequest) {
        Category category = categoryService.findCategoryByNameMongodbOperations(categoryRequest.getName());

        return new ResponseEntity<>(category, HttpStatus.OK);
    }
}
