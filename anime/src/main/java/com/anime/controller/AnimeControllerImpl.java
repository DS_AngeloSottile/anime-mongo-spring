package com.anime.controller;


import com.anime.model.Anime;
import com.anime.model.dto.AnimeResponseDto;
import com.anime.model.dto.AnimeStoreDto;
import com.anime.model.dto.AnimeUpdateRequestDto;
import com.anime.service.AnimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/anime")
public class AnimeControllerImpl implements AnimeController {

    @Autowired
    AnimeService animeService;

    @Override
    @PostMapping(value = "/storeAnime")
    public ResponseEntity<Anime> storeAnime(AnimeStoreDto animeRequest) {

        Anime anime = animeService.storeAnime(animeRequest);

        return new ResponseEntity<>(anime, HttpStatus.OK);
    }

    @Override
    @PostMapping(value = "/storeAnimeMongoOperations")
    public ResponseEntity<Map<String, Object>> storeAnimeMongoOperations(AnimeStoreDto animeRequest) {

        Map<String, Object> anime = animeService.storeAnimeMongoOperations(animeRequest);

        return new ResponseEntity<>(anime, HttpStatus.OK);
    }

    @Override
    @PostMapping(value = "/updateAnime")
    public ResponseEntity<AnimeResponseDto> updateAnimeByName(AnimeUpdateRequestDto animeRequest) {
        AnimeResponseDto animeDto = animeService.updateAnimeByName(animeRequest);

        return new ResponseEntity<>(animeDto,HttpStatus.OK);
    }

    @Override
    @GetMapping(value = "/findByNameMongodb/{name}")
    public ResponseEntity<List<AnimeResponseDto>> findByNameMongoDb(String name) {
        List<AnimeResponseDto> animeList = animeService.findByNameMongoDb(name);

        return new ResponseEntity<>(animeList,HttpStatus.OK);
    }

    @Override
    @GetMapping(value = "/findByEpisodesAggregation")
    public ResponseEntity<List<AnimeResponseDto>> findByEpisodes() {
        List<AnimeResponseDto> animeList = animeService.exampleAggregationFindByRange();

        return new ResponseEntity<>(animeList,HttpStatus.OK);
    }
}
