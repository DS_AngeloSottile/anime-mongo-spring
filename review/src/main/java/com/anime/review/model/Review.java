package com.anime.review.model;

import com.anime.review.model.dto.Anime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document("review")
public class Review {


    @Id
    private String id;
    @Indexed(unique=true)
    private String animeName;
    private String text;
    private String status;

}
