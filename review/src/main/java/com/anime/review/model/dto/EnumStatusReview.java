package com.anime.review.model.dto;

public enum EnumStatusReview {
    IN_PROGRESS,
    DONE
}
