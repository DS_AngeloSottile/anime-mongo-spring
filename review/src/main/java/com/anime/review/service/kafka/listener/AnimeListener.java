package com.anime.review.service.kafka.listener;


import com.anime.review.model.dto.Anime;
import com.anime.review.service.ReviewService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AnimeListener {
    
    Logger logger = LoggerFactory.getLogger(AnimeListener.class);

    
    @Autowired
    private ReviewService reviewService;
    @Autowired
    ObjectMapper objectMapper;
                                                                     //questo sarebbe l'input
    @KafkaListener(id = "#{'${spring.application.name}'}", topics = "#{'${ms.topic.anime.review.input}'}")
    public void listen(String animeDtoJsonString, Acknowledgment acknowledgment) {
        logger.info(animeDtoJsonString); //una volta arrivato l'ordine bisona deserializzarlo da json ad oggetto e richiamare il service che si vuole

        try {
            Anime anime = objectMapper.readValue(animeDtoJsonString, Anime.class);
            reviewService.createEmptyReviewByKafka(anime); //il service lo chiamo dal listner
        } catch (JsonProcessingException e) {
            String message = String.format("Send Event Error (%s)", Anime.class.getSimpleName());
            logger.error(message,e);
        }
        acknowledgment.acknowledge();
    } 
}
