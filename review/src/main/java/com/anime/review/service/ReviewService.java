package com.anime.review.service;

import com.anime.review.model.Review;
import com.anime.review.model.dto.Anime;

public interface ReviewService {

    Review createEmptyReviewByKafka(Anime anime);

    Review createTextReview(Review review);
}
