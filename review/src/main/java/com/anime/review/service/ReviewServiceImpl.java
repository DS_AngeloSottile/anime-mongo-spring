package com.anime.review.service;

import com.anime.review.model.Review;
import com.anime.review.model.dto.Anime;
import com.anime.review.model.dto.EnumStatusReview;
import com.anime.review.repository.ReviewRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReviewServiceImpl implements ReviewService {

    private final static Logger LOGGER = LoggerFactory.getLogger(ReviewServiceImpl.class);

    @Autowired
    ReviewRepository reviewRepository;

    public Review createEmptyReviewByKafka(Anime anime){

        Review review = new Review();

        review.setAnimeName(anime.getName());
        review.setStatus(EnumStatusReview.IN_PROGRESS.toString());
        review.setText("");

        return reviewRepository.save(review);
    }

    @Override
    public Review createTextReview(Review review) {
        return null;
    }
}
